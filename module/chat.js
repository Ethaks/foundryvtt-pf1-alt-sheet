// import { createCustomChatMessage } from "../../../systems/pf1/pf1.js";

export function addChatHooks() {
    libWrapper.register('pf1-alt-sheet', 'game.pf1.chat.ChatAttack.prototype.addDamage', addDamageWrapper, 'WRAPPER');
    libWrapper.register('pf1-alt-sheet', 'game.pf1.chat.ChatAttack.prototype.addAttack', addAttackWrapper, 'WRAPPER');

    // This is fucking ugly.
    libWrapper.register('pf1-alt-sheet', 'game.pf1.ItemAttack.postMessage', postMessage, 'OVERRIDE');
}

// Copied from pf1
async function postMessage(shared={}) {
    console.log("pf1-alt-sheet | postMessage");

    Hooks.call("itemUse", this, "postAttack", {
        ev: shared.event,
        skipDialog: shared.skipDialog,
        chatData: shared.chatData,
        templateData: shared.templateData,
      });

    // Create message
    const template = "modules/pf1-alt-sheet/templates/chat/attack-roll.hbs";
    shared.templateData.damageTypes = game.pf1.damageTypes.toRecord();

    // Show chat message
    let result;
    if (shared.chatAttacks.length > 0) {
        if (shared.chatMessage && shared.scriptData.hideChat !== true)
        result = await createCustomChatMessage(template, shared.templateData, shared.chatData);
        else result = { template: template, data: shared.templateData, chatData: shared.chatData };
    } else {
        if (shared.chatMessage && shared.scriptData.hideChat !== true) result = this.roll();
        else result = { descriptionOnly: true };
    }

    return result;
}

//Copied from pf1
async function createCustomChatMessage(
    chatTemplate,
    chatTemplateData = {},
    chatData = {},
    { rolls = [] } = {}
  ) {
    chatData = mergeObject(
      {
        user: game.user.id,
        type: CONST.CHAT_MESSAGE_TYPES.CHAT,
      },
      chatData
    );

    chatData.content = await renderTemplate(chatTemplate, chatTemplateData);

    // Handle different roll modes
    game.pf1.chat.ChatMessagePF.applyRollMode(chatData, chatData.rollMode ?? game.settings.get("core", "rollMode"));

    // Dice So Nice integration
    if (chatData.roll != null && rolls.length === 0) rolls = [chatData.roll];
    if (game.dice3d != null && game.dice3d.isEnabled()) {
      for (const roll of rolls) {
        await game.dice3d.showForRoll(roll, game.user, false, chatData.whisper, chatData.blind);
        chatData.sound = null;
      }
    }

    return game.pf1.chat.ChatMessagePF.create(chatData);
  };


async function addAttackWrapper(fnext, options = {}) {
    await fnext(options);

    let data = this.attack;
    let roll = data.roll;
    let d20 = roll.dice.length ? roll.dice[0].total : roll.terms[0].total;
    this.attack.nat20 = d20 === 20;
}

async function addDamageWrapper(fnext, options = {}) {
    await fnext(options);

    let data = this.damage;
    if (options.critical === true) data = this.critDamage;

    // Determine total damage
    let totalDamage = data.parts.reduce((cur, p) => {
        return cur + p.amount;
    }, 0);
    if (options.critical) {
        totalDamage = this.damage.parts.reduce((cur, p) => {
            return cur + p.amount;
        }, totalDamage);
    }

    // Handle minimum damage rule
    let minimumDamage = false;
    if (totalDamage < 1) {
        totalDamage = 1;
        minimumDamage = true;
    }

    // New cards
    if (options.critical)
    {
        this.cards.critical = {};
        this.cards.critical.items = [];
        if (this.isHealing) {
            this.cards.critical.items.push({
                label: "0.5x",
                value: -Math.floor(totalDamage / 2),
                action: "applyDamage",
            });
            this.cards.critical.items.push({
                label: "1x",
                value: -totalDamage,
                action: "applyDamage",
            });
            this.cards.critical.items.push({
                label: "1.5x",
                value: -Math.floor(totalDamage * 1.5),
                action: "applyDamage",
            });
        } else {
            this.cards.critical.items.push({
                label: "0.5x",
                value: Math.floor(totalDamage / 2),
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
            this.cards.critical.items.push({
                label: "1x",
                value: totalDamage,
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
            this.cards.critical.items.push({
                label: "1.5x",
                value: Math.floor(totalDamage * 1.5),
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
        }
    } else {
        this.cards.damage = {};
        this.cards.damage.items = [];
        if (this.isHealing) {
            this.cards.damage.items.push({
                label: "0.5x",
                value: -Math.floor(totalDamage / 2),
                action: "applyDamage",
            });
            this.cards.damage.items.push({
                label: "1x",
                value: -totalDamage,
                action: "applyDamage",
            });
            this.cards.damage.items.push({
                label: "1.5x",
                value: -Math.floor(totalDamage * 1.5),
                action: "applyDamage",
            });
        } else {
            this.cards.damage.items.push({
                label: "0.5x",
                value: Math.floor(totalDamage / 2),
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
            this.cards.damage.items.push({
                label: "1x",
                value: totalDamage,
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
            this.cards.damage.items.push({
                label: "1.5x",
                value: Math.floor(totalDamage * 1.5),
                action: "applyDamage",
                tags: minimumDamage ? "nonlethal" : "",
            });
        }
    }
}
